/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.richtercloud.document.scanner.performance.tests;

import de.richtercloud.document.scanner.gui.DocumentScannerUtils;
import de.richtercloud.document.scanner.test.entities.LargeBinaryEntity;
import de.richtercloud.message.handler.IssueHandler;
import de.richtercloud.message.handler.LoggerIssueHandler;
import de.richtercloud.reflection.form.builder.jpa.MemorySequentialIdGenerator;
import de.richtercloud.reflection.form.builder.jpa.retriever.JPAOrderedCachedFieldRetriever;
import de.richtercloud.reflection.form.builder.jpa.storage.PersistenceStorage;
import de.richtercloud.reflection.form.builder.jpa.storage.PostgresqlAutoPersistenceStorage;
import de.richtercloud.reflection.form.builder.jpa.storage.PostgresqlAutoPersistenceStorageConf;
import de.richtercloud.reflection.form.builder.retriever.FieldOrderValidationException;
import de.richtercloud.reflection.form.builder.storage.StorageConfValidationException;
import de.richtercloud.reflection.form.builder.storage.StorageCreationException;
import de.richtercloud.reflection.form.builder.storage.StorageException;
import de.richtercloud.test.tools.TestRandomUtils;
import de.richtercloud.validation.tools.FieldRetriever;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Shows that freeing of memory after {@link PersistenceStorage#shutdown() }
 * works well.
 *
 * @author richter
 */
public class LargeBinaryStorageIT {
    private final static Logger LOGGER = LoggerFactory.getLogger(LargeBinaryStorageIT.class);
    private final static String BIN_TEMPLATE = "bin";

    @Test
    @SuppressWarnings("PMD.JUnitTestsShouldIncludeAssert")
    public void testLargeBinaryStorage() throws IOException,
            StorageConfValidationException,
            StorageCreationException,
            StorageException,
            InterruptedException,
            FieldOrderValidationException {
        LOGGER.info("testLargeBinaryStorage");
        Locale.setDefault(Locale.ENGLISH);
        Set<Class<?>> entityClasses = new HashSet<>(Arrays.asList(LargeBinaryEntity.class));
        File databaseDir = Files.createTempDirectory("document-scanner-large-binary-it").toFile();
        FileUtils.forceDelete(databaseDir);
        File schemeChecksumFile = File.createTempFile("document-scanner-large-binary-it", null);
        schemeChecksumFile.delete();
        String persistenceUnitName = "document-scanner-it";
        String username = "document-scanner";
        String password = "document-scanner";
        String databaseName = "document-scanner";
        File postgresqlInstallationPrefixDir = Paths.get("/", "usr", "lib", "postgresql", "10").toFile();
        IssueHandler issueHandler = new LoggerIssueHandler(LOGGER);
        String initdb = new File(postgresqlInstallationPrefixDir,
                String.join(File.separator, BIN_TEMPLATE, "initdb")).getAbsolutePath();
        String postgres = new File(postgresqlInstallationPrefixDir,
                String.join(File.separator, BIN_TEMPLATE, "postgres")).getAbsolutePath();
        String createdb = new File(postgresqlInstallationPrefixDir,
                String.join(File.separator, BIN_TEMPLATE, "createdb")).getAbsolutePath();
        String pgCtl = new File(postgresqlInstallationPrefixDir,
                String.join(File.separator, BIN_TEMPLATE, "pg_ctl")).getAbsolutePath();
        PostgresqlAutoPersistenceStorageConf storageConf = new PostgresqlAutoPersistenceStorageConf(entityClasses,
                "localhost", //hostname
                username,
                password,
                databaseName,
                schemeChecksumFile,
                databaseDir.getAbsolutePath(),
                initdb, //initdbBinaryPath
                postgres, //postgresBinaryPath
                createdb, //createdbBinaryPath
                pgCtl
        );
        FieldRetriever fieldRetriever = new JPAOrderedCachedFieldRetriever(DocumentScannerUtils.QUERYABLE_AND_EMBEDDABLE_CLASSES);
        PersistenceStorage<Long> storage = null;
        try {
            storage = new PostgresqlAutoPersistenceStorage(storageConf,
                    persistenceUnitName,
                    1, //parallelQueryCount
                    fieldRetriever,
                    issueHandler
            );
            storage.start();
            int entityCount = 20;
            for(int i=0; i<entityCount; i++) {
                int mbSize = TestRandomUtils.getInsecureTestRandom().nextInt(64); //64 MB max.
                    //128 MB cause trouble on Travis CI (crash because of
                    //limited `vm.max_map_count` which causes
                    //`Native memory allocation (mmap) failed to map 109576192 bytes for committing reserved memory.`
                    //) and it's not worth figuring this out for now
                int byteCount = 1024*1024*mbSize;
                LOGGER.debug(String.format("generating %d MB random bytes", mbSize));
                byte[] largeRandomBytes = new byte[byteCount];
                TestRandomUtils.getInsecureTestRandom().nextBytes(largeRandomBytes);
                LargeBinaryEntity entity1 = new LargeBinaryEntity(largeRandomBytes);
                LOGGER.debug(String.format("storing large binary entity (%d of %d)", i, entityCount));
                entity1.setId(MemorySequentialIdGenerator.getInstance().getNextId(entity1));
                storage.store(entity1);
            }
            storage.shutdown();
            Thread.sleep(20000);
                //10000 causes
                //`Caused by: org.postgresql.util.PSQLException: FATAL: the database system is starting up`
            storage = new PostgresqlAutoPersistenceStorage(storageConf,
                    persistenceUnitName,
                    1, //parallelQueryCount
                    fieldRetriever,
                    issueHandler
            );
            storage.start();
            LOGGER.debug("querying large binary entity");
            storage.runQueryAll(LargeBinaryEntity.class);
            LOGGER.debug("query completed");
        }finally {
            if(storage != null) {
                storage.shutdown();
            }
        }
    }
}
